# Description
`baymax`, your personal terminal companion, implements common functions used when developing and adds extra terminal functionality. Some of the functions it handles is:

- `baymax docker` - `docker` additions and simplification of common `docker` tasks
- `baymax git` - `git` additions and simplification of common `git` tasks
- `baymax process` - simplification of common process management tasks
- `baymax var` - simple to use persistent key/value store for saving variables
- `baymax cmd` - simple to use persistent key/command store to save shortcuts / aliases to commonly used commands
- `baymax workspace` - macOS development setup and create and manage workspaces for Terminal on macOS
- `baymax utils` - A collection of miscellaneous common tasks that can be done through command line 

# Installation
To install, simply run:

`curl https://gitlab.com/ajayp/baymax/raw/master/install.sh | bash`

Once you have successfully installed `baymax`, running `baymax version` should yield the current installed version.

If you're on macOS, you can get all the tools `baymax` needs to run setup with `sudo -H baymax workspace install`.

### Troubleshooting
_Note: If you see any "Permission Denied." errors, you may need `sudo` access. Install with this command instead:_

`curl https://gitlab.com/ajayp/baymax/raw/master/install.sh | sudo bash && sudo chown -R $(whoami) /usr/local/`

# Usage
To find out what commmands can be run with `baymax` run `baymax help`.

To see what version you are on, run `baymax version`.

To update, run `baymax update`.

### Templating Syntax
When adding a command to `baymax cmd` you can use the following two syntax rules:

1. `{{0}}, {{1}}, {{2}}` will be replaced with the 1st, 2nd, and 3rd command line arguments to your command, or empty string if none are provided.
2. `{{shell script here}}` will be evaluated as a shell script and replaced with the output. For example, `{{ls}}` would be replaced with a string containing the list of files.

# Backup

Exporting and importing the settings/preferences/persistent data of `baymax` can be done by `baymax export <file>` and `baymax import <file>` respectively.

### Continous backup

A continuous backup to a file can be done by using `baymax backup <file>`. By keeping the file path in a cloud storage providers sync folder (like Dropbox, Google Drive, etc), you can have the backup sync to the cloud.

-------------------------------
# Some cool examples

### Setup your Mac for Development

### Terminal Workspaces

### Terminal Reset

### Smart Terminal Prompt with Git Status

### Git Copush

### Git Undo/Squash/Remove

### Workspace Install

